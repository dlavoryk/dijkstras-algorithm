#include "Graph.h"
#include <iostream>
#include <algorithm>


void GraphNode::VisitBy(const GraphNode & Another, const Type Cost)
{
	bIsVIsited = true;
	TotalLength = Another.TotalLength + Cost;
}

Graph::Graph(const Matrix<Type>& GraphAsMatrix)
{
	for (size_t i = 0; i < GraphAsMatrix.size(); ++i)
	{
		AddNode(i);
	}
	for (size_t i = 0; i < GraphAsMatrix.size(); ++i)
	{
		for (size_t j = 0; j < GraphAsMatrix.size(); ++j)
		{
			const Type & El = GraphAsMatrix[i][j];
			if (El >= 0)
			{
				mRoots[i]->AddTransition({ mRoots[j].get(), El });
			}
		}
	}
}

void Graph::FindPath(int IdXFrom, int IdxTo)
{
	ResetForPathFinding();
	
	std::vector<int> VisitedIndexes;
	VisitedIndexes.push_back(IdXFrom);
	Find(mRoots[IdXFrom].get(), mRoots[IdxTo].get(), VisitedIndexes);
	
	std::cout << "Length from " << IdXFrom << " to " << IdxTo << " = " << mRoots[IdxTo]->TotalLength << std::endl;
	std::cout << "points: " << std::endl;
	for (const int & El : mSearchResult)
	{
		std::cout << El << " " << std::endl;
	}
}

void Graph::Find(GraphNode * From, GraphNode * To, std::vector<int> & VisitedIndexes)
{
	if (From != To)
	{
		for (Connection & Transition : From->Transitions)
		{
			if (// Check if this Node might be useful
				(!Transition.Node->bIsVIsited || Transition.Node->TotalLength > From->TotalLength + Transition.Cost)
				&&// Check if we did not start search with this objects
				std::find(VisitedIndexes.begin(), VisitedIndexes.end(), Transition.Node->Idx) == VisitedIndexes.end()
				)
			{
				Transition.Node->VisitBy(*From, Transition.Cost);
				VisitedIndexes.push_back(Transition.Node->Idx);
				Find(Transition.Node, To, VisitedIndexes);
				VisitedIndexes.erase(std::find(VisitedIndexes.begin(), VisitedIndexes.end(), Transition.Node->Idx), VisitedIndexes.end());
			}
		}
	}
	else
	{
		mSearchResult = VisitedIndexes;
		mSearchResult.pop_back();
	}
}

void Graph::AddNode(int Idx)
{
	mRoots.push_back(std::make_unique<GraphNode>(Idx));
}

void Graph::ResetForPathFinding()
{
	mSearchResult.erase(mSearchResult.begin(), mSearchResult.end());
	for (auto & Node : mRoots)
	{
		Node->TotalLength = 0;
		Node->bIsVIsited = false;
	}
}
