#pragma once
#include <vector>
#include <memory>
typedef int Type;

template <typename T>
using Matrix = std::vector<std::vector<T>>;

struct GraphNode;

struct Connection
{
	GraphNode * Node;
	Type Cost;
	
	Connection() = delete;
	Connection(GraphNode * ConnectedNode, Type ConnectionCost) : Node(ConnectedNode), Cost(ConnectionCost){}

	bool IsValid() const { return Node != nullptr; }
};

struct GraphNode
{
	std::vector<Connection> Transitions;

	const int Idx;

	Type TotalLength = 0;
	bool bIsVIsited = false;

	void AddTransition(Connection && NewTransition) { Transitions.emplace_back(NewTransition); }
	void AddTransition(const Connection & NewTransition) { Transitions.emplace_back(NewTransition); }

	void VisitBy(const GraphNode & Another, const Type Cost);

	GraphNode(int InIdx) : Idx(InIdx) {}
	GraphNode() = delete;
};

typedef std::unique_ptr<GraphNode> NodePtr;

class Graph
{
public:
	
	Graph(const Matrix<Type> & GraphAsMatrix);

	void FindPath(int IdXFrom, int IdxTo);

	void Find(GraphNode * From, GraphNode * To, std::vector<int> & VisitedIndexes);
	
protected:
	void AddNode(int Idx);
	void ResetForPathFinding();

protected:
	std::vector<int> mSearchResult;

	std::vector<NodePtr> mRoots;
};

