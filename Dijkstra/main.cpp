#include <iostream>

#include "Graph.h"

bool IsSimetric (const Matrix<int> & Val)
{
	for (size_t i = 0; i < Val.size(); ++i)
	{
		for (size_t j = 0; j < Val.size(); ++j)
		{
			if (Val[i][j] != Val[j][i])
			{
				/// or goto or throw
				return false;
			}
		}
	}
	return true;
}

int main()
{
	const Matrix<int> GraphAsMatrix =
	{
		{-1,  2, -1,  4, -1, -1},
		{ 2, -1,  5,  1,  2, -1},
		{-1,  5, -1,  1,  0,  3},
		{ 4,  1,  1, -1,  3, -1},
		{-1,  2,  0,  3, -1,  4},
		{-1, -1,  3, -1,  4, -1}
	};

	//const Matrix<int> GraphAsMatrix =
	//{
	//	{-1,  2,  5, 10},
	//	{ 2, -1,  1,  5},
	//	{ 5,  1, -1,  3},
	//	{10,  5,  3, -1}
	//};
	
	std::cout << IsSimetric(GraphAsMatrix) << std::endl;
	
	Graph G(GraphAsMatrix);
	G.FindPath(0, 3);

	system("pause");
}